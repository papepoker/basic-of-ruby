class PageController < ApplicationController

  def login
    flash[:message] = ''
    
    if params[:member]
      user = params[:member][:username]
      pass = params[:member][:password]

      if user == 'admin' and pass == '1234'
        session[:member] = 'Tavon'
      else
        flash[:message] = 'username ผิด'
      end
    end
  # def login
  end

  def index
    if params[:product]
      if params[:product][:search]
      search = params[:product][:search]
      @products = Product.where("pro_name LIKE('%" + search +"%')").order('pro_price').all
      else
      flash[:message] = 'Word for search can\'t be blank'
      # end if params[:product][:search]
      end
    else
      @products = Product.all
      # end if params[:product][:search]
    end
  # end def index
  end

  def product_new
    if params[:product]

      product = Product.new
      product.pro_name = params[:product][:pro_name]
      product.pro_code = params[:product][:pro_code]
      product.pro_price = params[:product][:pro_price]

      #check image file
      if params[:product][:pro_img]

        img = params[:product][:pro_img]
        name = img.original_filename
        ext = name[name.length - 3, 3]

        newName = DateTime.now.to_s + "." + ext

        ext_pass = ['png','jpg','gif']

        if ext_pass.include?(ext)
          path = File.join("public/upload", newName)
          File.open(path, "wb") { |f| f.write(img.read) }
        else
          flash[:message] = 'You can upload file of picture (.png , .jpg , gif) only'
        # end if ext_pass.include?(ext)
        end

        product.pro_img = newName
      # end if params[:product][:pro_img]
      end

      if product.save
          flash[:message] = 'Save success'
          redirect_to :action => 'index'
      # end if product.save
      end
    # end if params[:product]
    end

  # end def product_new
  end

  def delete
        if params[:id]
          product = Product.find(params[:id])
          if product
          File.delete('public/upload/' + product.pro_img)
          Product.delete(params[:id])

          flash[:message] = 'Delete success'
          redirect_to :action => 'index'
          # end if product
          end
        # end if params[:id]
        end 
  # end def delete
  end

  def edit
      if params[:id]
        @product = Product.find(params[:id])
        render 'product_new'
      # end if params[:id]
      end

      if params[:product]
          product = Product.find(params[:product][:pro_id])

          product.pro_name = params[:product][:pro_name]
          product.pro_code = params[:product][:pro_code]
          product.pro_price = params[:product][:pro_price]

            #check image file
            if params[:product][:pro_img]

          img = params[:product][:pro_img]
          name = img.original_filename
          ext = name[name.length - 3, 3]

          newName = DateTime.now.to_s + "." + ext

          ext_pass = ['png','jpg','gif']

          if ext_pass.include?(ext)
          File.delete('public/upload/' + product.pro_img)
          path = File.join("public/upload", newName)
          File.open(path, "wb") { |f| f.write(img.read) }
          else
          flash[:message] = 'You can upload file of picture (.png , .jpg , gif) only'
          # end if ext_pass.include?(ext)
          end

        product.pro_img = newName
      # end if params[:product][:pro_img]
      end

          if product.save
          flash[:message] = 'Save success'
          redirect_to :action => 'index'
          # end if product.save
          end
      # end if params[:product]
      end 
  # end def edit
  end

  # end class PageController
end









