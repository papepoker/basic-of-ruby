Rails.application.routes.draw do
  # Page models
  get 'page/index'
  get 'page/about'
  get 'page/product_new'
  get 'page/login'
  get 'page/register'
  get 'page/delete'
  get 'page/edit'
  get 'page/buy'

  post 'page/login'
  post 'page/product_new'
  post 'page/edit'
  post 'page/index'


  root to: 'page#index'
end
